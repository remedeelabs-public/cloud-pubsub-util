package main

import (
	"context"
	"flag"

	"cloud.google.com/go/pubsub"
	"github.com/golang/glog"
)

var (
	pubsubProjectID        string
	pubsubTopicName        string
	pubsubSubscriptionName string
)

func init() {
	flag.StringVar(&pubsubProjectID, "project", "", "Pub/Sub project ID")
	flag.StringVar(&pubsubTopicName, "topic", "", "Pub/Sub topic name")
	flag.StringVar(&pubsubSubscriptionName, "subscription", "", "Pub/Sub subscription name")
	flag.Parse()
}

func main() {
	ctx := context.Background()

	pubsubClient, err := pubsub.NewClient(ctx, pubsubProjectID)
	if err != nil {
		glog.Fatalf("Error creating PubSub client: %v", err)
	}
	glog.Info("Successfully created PubSub client")

	topic := pubsubClient.Topic(pubsubTopicName)
	ok, err := topic.Exists(ctx)
	if err != nil {
		glog.Fatalf("Error finding topic: %v", err)
	}
	if !ok {
		glog.Fatalf("Topic %s doesn't exist", pubsubTopicName)
	}
	_, err = pubsubClient.CreateSubscription(ctx, pubsubSubscriptionName,
		pubsub.SubscriptionConfig{Topic: topic})
	if err != nil {
		glog.Fatalf("Error creating subscription: %v", err)
	}
	glog.Infof("Created subscription %s", pubsubSubscriptionName)
}
