package main

import (
	"context"
	"flag"

	"cloud.google.com/go/pubsub"
	"github.com/golang/glog"
)

var (
	pubsubProjectID string
	pubsubTopicName string
)

func init() {
	flag.StringVar(&pubsubProjectID, "project", "", "Pub/Sub project ID")
	flag.StringVar(&pubsubTopicName, "topic", "", "Pub/Sub topic name")
	flag.Parse()
}

func main() {
	ctx := context.Background()

	pubsubClient, err := pubsub.NewClient(ctx, pubsubProjectID)
	if err != nil {
		glog.Fatalf("Error creating PubSub client: %v", err)
	}
	glog.Info("Successfully created PubSub client")

	ok, err := pubsubClient.Topic(pubsubTopicName).Exists(ctx)
	if err != nil {
		glog.Fatalf("Error finding topic: %v", err)
	}
	if ok {
		glog.Infof("Topic %s already exists", pubsubTopicName)
		return
	}
	_, err = pubsubClient.CreateTopic(ctx, pubsubTopicName)
	if err != nil {
		glog.Fatalf("Error creating topic: %v", err)
	}
	glog.Infof("Created topic %s", pubsubTopicName)
}
