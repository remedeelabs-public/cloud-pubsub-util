module cloud-pubsub-util

go 1.13

require (
	cloud.google.com/go/pubsub v1.4.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
)
